package com.sunshin.ssh.service;

import com.sunshine.ssh.entity.UserInfo;
import com.sunshine.ssh.service.IUserInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Author: WeiHong
 * @Date: 2017/6/7 16:03
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/spring-application.xml",
        "classpath:spring/spring-hibernate.xml"})
public class TestUserInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUserInfoService.class);

    @Autowired
    private IUserInfoService userInfoService;

    @Test
    public void save(){
        LOGGER.info("测试开始.......");
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName("weihong");
        userInfo.setUserPassword("1234");
        userInfo.setGender(0);
        userInfo.setUserAge(20);
        Integer i = userInfoService.save(userInfo);
    }
}
