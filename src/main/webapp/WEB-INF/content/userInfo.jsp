<%--
  Created by IntelliJ IDEA.
  User: WeiHong
  Date: 2017/6/8
  Time: 10:25
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <style>

        h1,h3 {
            text-align: center;
        }
        table {
            margin: 20px auto;
        }

        td{
            width:130px;
            text-align: center;
        }
    </style>
</head>
<body>
<h1>全部用户信息：</h1>
    <h3><a href="add.action">新增用户</a></h3>
<table border="1px solid #ccc" cellpadding="0" cellspacing="0">
    <tr>
        <th>姓名</th>
        <th>年龄</th>
        <th>电话</th>
        <th>详细</th>
    </tr>
    <c:forEach items="${userInfos}" var="user">
        <tr>
            <td>${user.userName}</td>
            <td>${user.userAge}</td>
            <td>${user.gender}</td>
            <td><a target="_blank" href="userInfo.action?id=${user.userId}">json详情</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
