<%--
  Created by IntelliJ IDEA.
  User: WeiHong
  Date: 2017/6/6
  Time: 15:10
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>用户注册</title>
</head>
<body>
<s:form action="register">
    <s:textfield name="userInfo.userName" label="用户名"/>
    <s:password name="userInfo.userPassword" label="密码"/>
    <s:select list="#{'1':'男','0':'女'}" listKey="key" listValue="value" name="userInfo.gender" label="性别"
              value="1">
    </s:select>
    <s:submit value="注册" />
</s:form>

</body>
</html>
