package com.sunshine.ssh.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.sunshine.ssh.entity.UserInfo;
import com.sunshine.ssh.service.IUserInfoService;
import com.sunshine.ssh.util.AjaxUtil;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author: WeiHong
 * @Date: 2017/6/7 16:33
 */
@Namespace("/user")
public class UserInfoAction extends ActionSupport implements ModelDriven<UserInfo>,Preparable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoAction.class);

    private Integer id;
    private UserInfo userInfo;
    private List<UserInfo> userInfos;

    @Autowired
    private IUserInfoService userInfoService;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    @Override
    public UserInfo getModel() {
        if (null != id) {
            userInfo = userInfoService.get(id);
        } else {
            userInfo = new UserInfo();
        }
        return userInfo;
    }

    @Action(value = "list",results = {@Result(name="success",location="/WEB-INF/content/userInfo.jsp")})
    public String list() throws Exception {
        LOGGER.info("查询所有用户");
        userInfos = userInfoService.findAll();
        return SUCCESS;

    }

    @Action(value = "add",results = {@Result(name="success",location="/WEB-INF/content/addUserInfo.jsp")})
    public String add() throws Exception {
        return SUCCESS;
    }


    public void detail() {
        String id = ServletActionContext.getRequest().getParameter("id");
        LOGGER.info("查看用户详情：" + id);
        userInfo = userInfoService.get(Integer.valueOf(id));
        AjaxUtil.ajaxJSONResponse(userInfo);
    }

    @Override
    public void prepare() throws Exception {

    }
}
