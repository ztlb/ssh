package com.sunshine.ssh.hibernate;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import javax.imageio.spi.ServiceRegistry;

/**
 * @Author: WeiHong
 * @Date: 2017/6/7 14:33
 */
public class HibernateSessionFactory {
    private static final String CFG_FILE_LOCATION = "Hibernate.cfg.xml";

    private static final ThreadLocal<Session> THREAD_LOCAL = new ThreadLocal<>();

    private static final Configuration cfg = new Configuration().configure(CFG_FILE_LOCATION);

    private static ServiceRegistry registry;



}
