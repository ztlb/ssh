package com.sunshine.ssh.util;

import com.alibaba.fastjson.JSON;
import org.apache.struts2.ServletActionContext;

import java.io.IOException;

/**
 * @Author: WeiHong
 * @Date: 2017/6/7 16:40
 */
public class AjaxUtil {

    // ajax返回text
    public static void ajaxResponse(String text) {
        try {
            ServletActionContext.getResponse().setContentType(
                    "text/html;charset=utf-8");
            ServletActionContext.getResponse().getWriter().write(text);
            ServletActionContext.getResponse().getWriter().flush();
            ServletActionContext.getResponse().getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ajaxJSON返回
    public static void ajaxJSONResponse(Object object) {
        try {
            String json = JSON.toJSONStringWithDateFormat(object,
                    "yyyy-MM-dd HH:mm:ss");
            ServletActionContext.getResponse().setContentType(
                    "text/html;charset=utf-8");
            ServletActionContext.getResponse().getWriter().write(json);
            ServletActionContext.getResponse().getWriter().flush();
            ServletActionContext.getResponse().getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
