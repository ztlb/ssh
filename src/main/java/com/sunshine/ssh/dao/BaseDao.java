package com.sunshine.ssh.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: WeiHong
 * @Date: 2017/6/7 15:34
 */
public interface BaseDao<T, PK extends Serializable> {

    T load(PK id);

    T get(PK id);

    List<T> findAll();

    void persist(T entity);

    PK save(T entity);

    void saveOrUpdate(T entity);

    void delete(PK id);

    void flush();
}
