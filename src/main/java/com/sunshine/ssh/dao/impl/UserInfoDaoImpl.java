package com.sunshine.ssh.dao.impl;

import com.sunshine.ssh.dao.IUserInfoDao;
import com.sunshine.ssh.entity.UserInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by WH on 2017/6/6.
 */
@Repository("userInfoDao")
public class UserInfoDaoImpl implements IUserInfoDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession(){
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public UserInfo load(Integer id) {
        return (UserInfo) this.getCurrentSession().load(UserInfo.class,id);
    }

    @Override
    public UserInfo get(Integer id) {
        return (UserInfo) this.getCurrentSession().load(UserInfo.class, id);
    }

    @Override
    public List<UserInfo> findAll() {
        List<UserInfo> list = this.getCurrentSession().createQuery("from UserInfo").list();
        return list;
    }

    @Override
    public void persist(UserInfo entity) {
        this.getCurrentSession().persist(entity);
    }

    @Override
    public Integer save(UserInfo entity) {


        return (Integer) this.getCurrentSession().save(entity);

        //return this.getHibernateTemplate().save(entity);
    }

    @Override
    public void saveOrUpdate(UserInfo entity) {
        this.getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Integer id) {
        this.getCurrentSession().delete(id);
    }

    @Override
    public void flush() {
        this.getCurrentSession().flush();
    }
}
