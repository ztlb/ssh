package com.sunshine.ssh.service;

import com.sunshine.ssh.entity.UserInfo;

import java.util.List;

/**
 * @Author: WeiHong
 * @Date: 2017/6/6 15:43
 */
public interface IUserInfoService {

    UserInfo load(Integer id);

    UserInfo get(Integer id);

    List<UserInfo> findAll();

    void persist(UserInfo entity);

    Integer save(UserInfo entity);

    void saveOrUpdate(UserInfo entity);

    void delete(Integer id);

    void flush();
}
