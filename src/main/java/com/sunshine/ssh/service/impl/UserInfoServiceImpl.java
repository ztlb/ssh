package com.sunshine.ssh.service.impl;

import com.sunshine.ssh.dao.IUserInfoDao;
import com.sunshine.ssh.entity.UserInfo;
import com.sunshine.ssh.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: WeiHong
 * @Date: 2017/6/6 15:44
 */
@Service("UserInfoService")
public class UserInfoServiceImpl implements IUserInfoService {

    @Autowired
    private IUserInfoDao userInfoDao;

    @Override
    public UserInfo load(Integer id) {
        return userInfoDao.load(id);
    }

    @Override
    public UserInfo get(Integer id) {
        return userInfoDao.get(id);
    }

    @Override
    public List<UserInfo> findAll() {
        return userInfoDao.findAll();
    }

    @Override
    public void persist(UserInfo entity) {
        userInfoDao.persist(entity);
    }

    @Override
    public Integer save(UserInfo entity) {
        return userInfoDao.save(entity);
    }

    @Override
    public void saveOrUpdate(UserInfo entity) {
        userInfoDao.saveOrUpdate(entity);
    }

    @Override
    public void delete(Integer id) {
        userInfoDao.delete(id);
    }

    @Override
    public void flush() {
        userInfoDao.flush();
    }
}
